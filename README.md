
# Expandable Notes Plugin for MSM

This plugin automatically loads all notes in a request, then collapses all but the first. You can click a note header to expand that note.

## Localisation

There is a string in the HTML file that controls the label of the Expand/Fold all checkbox. This need to be edited prior to installation.

## Compatible Versions

| Plugin  | MSM               |
|---------|-------------------|
| 1.1.6   | 14.10.5 - 15.0.0  |
| 1.1.7   | 15.1.0  - 15.x.x  |

## Installation

Please see your MSM documentation for information on how to install plugins.

Once the plugin has been installed The following will happen:

+ This plugin automatically loads all notes in a request, then collapses all but the first. You can click a note header to expand that note.

## Usage

The plugin is automatically loaded when you load a request.

## Contributing

We welcome all feedback including feature requests and bug reports. Please raise these as issues on GitHub. If you would like to contribute to the project please fork the repository and issue a pull request.
